'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

module.exports = {
  async find(ctx) {
    let entities;
    let statusses = [];

    // Everyone authenticated can see
    statusses.push('published');
    // Check the roles
    if (ctx.state && ctx.state.user && ctx.state.user.roles) {
      // Check if author
      if (ctx.state.user.roles.filter(e => e.code === 'strapi-author').length > 0) {
        if (!statusses.includes('draft')) {
          statusses.push('draft');
        }
      }
      // Check if Admin or Editor
      if (ctx.state.user.roles.filter(e => e.code === 'strapi-editor').length > 0 || ctx.state.user.roles.filter(e => e.code === 'strapi-admin').length > 0) {
        if (!statusses.includes('draft')) {
          statusses.push('draft');
        }
        if (!statusses.includes('archived')) {
          statusses.push('archived');
        }
      }
    }

    ctx.query = {
      ...ctx.query,
      status_in: statusses
    };

    if (ctx.query._q) {
      entities = await strapi.services.article.search(ctx.query);
    } else {
      entities = await strapi.services.article.find(ctx.query);
    }

    return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.article }));
  },
};
